@extends ('layouts.app')

@section('content')

<div class="card" style="width:75%; margin:auto;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                <h3 class="text-center">Let's start something new !</h3>
                <form
                method="POST"
                action="/projects">
                    @include ('projects.form', [
                        'project' => new App\Project,
                        'buttonText' => 'Create Project'
                        ])
                </form>
            </div>
        </div>
    </div>
</div>

  @endsection
