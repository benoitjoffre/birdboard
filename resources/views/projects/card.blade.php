<div class="card" style="height: 200px; width: auto;">
  <h3 class=" font-weight-normal text-lg mb-3 px-3 py-3">
    <a href="{{ $project->path() }}" class="text-black-100 no-underline">{{ $project->title }}</a>
  </h3>

  <div class="text-gray-500 pl-3">{{ str_limit($project->description, 100) }}</div>
</div>