@extends ('layouts.app')

@section('content')

<div class="card" style="width:75%; margin:auto;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-body">
                <h3 class="text-center">Edit Your Project</h3>
                <form
                method="POST"
            action="{{$project->path()}}">
            @method('PATCH')
                    @include ('projects.form', ['buttonText' => 'Update Project'])
                </form>
            </div>
        </div>
    </div>
</div>

  @endsection
