

                    @csrf

                    <div class="form-group">
                      <label for="title" class="">Title</label>

                      <div class="control">
                      <input id="title" name="title" type="text" class="form-control" value="{{$project->title}}" required>
                      </div>
                    </div>

                    <div class="form-group">
                        <label for="description" class="label">Description</label>

                        <div class="control">
                            <textarea id="description" name="description" type="text" class="form-control" required>{{$project->description}}</textarea>
                        </div>
                    </div>



                <button type="submit" class="btn btn-primary">{{ $buttonText }} </button>
                <a href="{{$project->path()}}">Cancel</a>

                @if ($errors->any())
                    <div class="alert alert-danger mt-4" role="alert">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error}}</li>
                        @endforeach
                    </div>
                @endif
