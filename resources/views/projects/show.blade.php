@extends ('layouts.app')

@section('content')
<header class="d-flex align-items-center mb-3 py-4">
  <div class="d-flex justify-content-between align-items-center w-100 mx-4">
    <p class="text-secondary text-sm font-weight-normal">
		<a href="/projects" >My Projects</a> / {{ $project->title }}
	</p>
<a href="{{ $project->path(). '/edit'}}" class="btn btn-primary">Edit Project</a>
  </div>
</header>

  <main>

	<div class="main-div w-100">
		<div class="row mx-3">
			<div class="col-lg-8">
				<div class="left w-100">
					<h2 class="text-secondary font-weight-normal text-xl mb-2">Tasks</h2>
					{{-- tasks --}}

					@foreach($project->tasks as $task)
						<div class="card mb-3 d-flex justify-content-center p-3" style="height: 50px;">
						<form action="{{ $task->path() }}" method="post">
								@method('PATCH')
								@csrf

								<div class="d-flex align-items-center justify-content-between">
								<input name="body" value="{{ $task->body }}" class="w-100 form-control border-0 p-0 mr-3 {{ $task->completed ? 'text-secondary' : '' }}">
									<input type="checkbox" name="completed" onChange="this.form.submit()" {{ $task->completed ? 'checked' : '' }}>
								</div>
							</form>
						</div>
					@endforeach

						<div>

							<form action="{{ $project->path() . '/tasks' }}" method="POST">
								@csrf

								<input placeholder="Add a new task..." class=" form-control w-100 p-3  mb-3" style="height: 50px;" name="body">
							</form>

						</div>


						<h2 class="text-secondary font-weight-normal text-lg mb-2">General Notes</h2>
									{{-- General notes --}}

							<form method="POST" action="{{$project->path()}}">
								@csrf
								@method('PATCH')

								<textarea
								name="notes"
								class="form-control p-3 w-100 mb-4"
								style="min-height: 150px; text-align: left;"
								placeholder="Anything special that you want to make a note of ?"
								>{{$project->notes}}</textarea>

								<button type="submit" class="btn btn-primary">Save</button>

                            </form>

                            @if ($errors->any())
                                <div class="alert alert-danger mt-4" role="alert">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error}}</li>
                                    @endforeach
                                </div>
                            @endif
				</div>
			</div>
				<div class="col-lg-4">
					<div class="right mt-4">
						@include ('projects.card')
					</div>
				</div>
		</div>
	</div>

  </main>
@endsection
