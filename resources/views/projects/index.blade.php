@extends ('layouts.app')

@section('content')

  <header class="d-flex align-items-center mb-3 py-4">
    <div class="d-flex justify-content-between align-items-center w-100 mx-4">
      <h2 class="text-sm text-gray-600 font-normal">My Projects</h2>
      <a href="/projects/create" class="btn btn-primary">New Project</a>
    </div>
  </header>

  <main class="flex flex-wrap -mx-4">
    @forelse ($projects as $project)
    <div class="px-4 pb-6">
      @include ('projects.card')
    </div>
      @empty
        <div>No projects yet.</div>
      @endforelse
  </main>
@endsection